# Official KiCad Addon Metadata Repository

This repository contains the source files used to generate the
[public-facing addons repository](https://gitlab.com/kicad/addons/repository) used by the Plugin
and Content Manager in KiCad.

Please read all instructions at https://dev-docs.kicad.org/en/addons/ before submitting merge
requests to this repository. Submitted packages must meet a number of requirements outlined there.